const express = require("express");
const bodyParser = require("body-parser");
const puppeteer = require("puppeteer");
const genericPool = require("generic-pool");
const cors = require("cors");

const app = express();
const port = process.env.PORT || 5001;
app.listen(port, () => console.log(`Server started on port: ${port}`));

const factory = {
	create: async function() {
		const browser = await puppeteer.launch({
			headless: true,
			args: [
				"--no-sandbox",
				"--disable-setuid-sandbox",
				"--disable-dev-shm-usage",
				"--single-process" // disable this in localhost
			]
		});

		const page = await browser.newPage();
		await page.setViewport({ width: 800, height: 420 });
		return page;
	},
	destroy: function(puppeteer) {
		puppeteer.close();
	}
};

const browserPool = genericPool.createPool(factory, {
	max: 10,
	min: 2,
	maxWaitingClients: 50
});

generatePDF = async url => {
	const page = await browserPool.acquire();
	await page.goto(url);
	await page.waitForSelector(".page");
	const buffer = await page.pdf({
		format: "A4",
		margin: { left: "2cm", top: "2.5cm", right: "2cm", bottom: "2.5cm" }
	});
	console.log("[PDF] Success => Id: ", url, "\n");
	await browserPool.release(page);

	return buffer;
};

app.use(cors());
app.use(bodyParser.json({ limit: "50mb" }));
app.use(
	bodyParser.urlencoded({
		limit: "50mb",
		extended: true
	})
);

app.post("/pdf", (req, res) => {
	let url = req.body.url;
	console.log("received request", url);
	console.log("Requested: ", new Date().toLocaleString());
	const pdfPromise = generatePDF(url);
	pdfPromise
		.then(buffer => {
			res.type("application/pdf");
			res.send(buffer);
			console.log("Delivered : ", new Date().toLocaleString());
		})
		.catch(error => console.log(error));
});
